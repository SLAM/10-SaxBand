# SaxBand

Ce projet n'est pas un groupe de Jazz, mais une application démontrant
le *parsing* XML à l'aide de la librairie SAX en Java.

## Introduction

SAX est l'acronyme de Simple API for XML. Cette API permet d'implémenter
une analyse évènementielle d'un document XML. Ainsi lors de la lecture
d'un document XML, chaque élément lexical du document XML (balise, début
ou fin du document) provoquera un évènement de la part du parseur et pourra
ainsi être traité par le gestionnaire (ou *handler* en anglais).

> Un parseur est un programme informatique réalisant une analyse syntaxique.

SAX fournit la classe `DefaultHandler`, qui nous permet de spécialiser
notre gestionnaire d'évènements.

## Gestionnaire de groupes de musique

La classe `BandHandler` implémente un tel gestionnaire (et spécialise donc
la classe `DefaultHandler`) :

* La surcharge de la méthode `startElement` permet d'implémenter les actions
à réaliser lors de la détection d'un nouvel élément lexical par le parseur.
* La surcharge de la méthode `endElement` permet d'implémenter les actions
à réaliser lors de la détection de la fin d'un élément lexical par le parseur.
* La surcharge de la méthode `characters` permet d'implémenter les actions
à réaliser sur les données.

Le *package* `Modele` implémente les classes métiers `Band` et `Album̀`,
liées par une association de type "1 à plusieurs".

Dans les ressources du projet, le document `bands.xml` propose un jeu
d'essais illustrant deux groupes de la scène indie-rock états-unienne.
