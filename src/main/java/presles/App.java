/*
 * Main class of a sax band parser.
 * 
 * @author moulinux
 * @version 0.1
 * @license GPLv3
 */
package presles;

import java.io.InputStream;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import presles.xml.BandHandler;

public class App {
    public static void main(String[] args) {        
        try {
            // Créer une instance du parseur à partir de la fabrique de parseurs de SAX
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            // Parser le document
            BandHandler bandHandler = new BandHandler();
            InputStream inputStream = App.class.getClassLoader().getResourceAsStream("bands.xml");
            parser.parse(inputStream, bandHandler);
            // Afficher la collection parsée
            System.out.println(bandHandler.getBands());
        }
        catch(Exception e){System.out.println(e);}
    }
}
