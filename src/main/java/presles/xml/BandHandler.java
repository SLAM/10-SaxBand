/*
 * Custom handler class for a xml band resource.
 * 
 * @author moulinux
 * @version 0.1
 * @license GPLv3
 */
package presles.xml;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import presles.Modele.Album;
import presles.Modele.Band;

public class BandHandler extends DefaultHandler {
    
    private final ArrayList<Band> bands;
    private Band band;
    private String name;
    private String description;
    private ArrayList<Album> albums;
    private Album album;
    private String title;
    private String year;
    private String label;
    private String node = null;

    /**
     * Constructeur par défaut
     */
    public BandHandler() {
        this.bands = new ArrayList();
    }

    /**
     * Actions à réaliser lors de la détection d'un nouvel élément.
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws SAXException 
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        
        //Nous stockons le nom du nœud pour gérer l'endroit ou affecter la valeur du nœud
        node = qName;
        // Dès que nous rencontrons un élément recherché, nous créons l'objet associé
        switch(qName) {
            case "band":
                this.band = new Band();
                this.name = new String();
                this.description = new String();
                break;
            case "albums":
                this.albums = new ArrayList();
                break;
            case "album":
                this.album = new Album();
                this.title = new String();
                this.year = new String();
                this.label = new String();
                break;
        }
    }

    /**
     * Actions à réaliser lors de la détection de la fin d'un élément.
     * @param uri
     * @param localName
     * @param qName
     * @throws SAXException 
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        // Lorsque nous détectons la fin d'un élément,
        // nous l'ajoutons à son objet parent
        switch(qName) {
            case "band":
                this.band.setName(this.name);
                this.band.setDescription(this.description);
                this.band.setAlbums(this.albums);
                // On ajoute à la collection locale, notre nouvelle instance
                this.bands.add(this.band);
                System.out.println("new band " + this.bands.size());
                break;
            case "album":
                this.album.setTitle(this.title);
                this.album.setYear(this.year);
                this.album.setLabel(this.label);
                // On ajoute à la collection locale, notre nouvelle instance
                this.albums.add(this.album);
                System.out.println("new album " + this.albums.size());
                break;
        }
    }
    
    /**
     * Actions à réaliser au début du document XML.
     * @throws SAXException 
     */
    @Override
    public void startDocument() throws SAXException {
        System.out.println("Debut du document");
    }

    /**
     * Actions à réaliser lors de la fin du document XML.
     * @throws SAXException 
     */
    @Override
    public void endDocument() throws SAXException {
        System.out.println("Fin du document");
    }
    
    /**
     * Actions à réaliser sur les données
     * @param ch
     * @param start
     * @param length
     * @throws SAXException 
     */
     @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String str = new String(ch, start, length);
        
        // Il n'y a des valeurs de nœud que pour ces cas là
        // Dès que nous les rencontrons, nous stockons la valeur dans l'objet ad hoc
        switch (node) {
            case "name":
                this.name  += str;
                break;
            case "description":
                this.description  += str;
                break;
            case "title":
                this.title  += str;
                break;
            case "year":
                this.year  += str;
                break;
            case "label":
                this.label  += str;
                break;
        }
    }
    
    public ArrayList<Band> getBands() {
        return this.bands;
    }
}
