/*
 * Model class for an album.
 * 
 * @author moulinux
 * @version 0.1
 * @license GPLv3
 */
package presles.Modele;

public class Album {
    
    private String title;
    private String year;
    private String label;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Album{" + "title=" + title + ", year=" + year + ", label=" + label + '}';
    }
}