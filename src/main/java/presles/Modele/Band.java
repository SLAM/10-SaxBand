/*
 * Model class for a band.
 * 
 * @author moulinux
 * @version 0.1
 * @license GPLv3
 */
package presles.Modele;

import java.util.ArrayList;

public class Band {
    
    private String name;
    private ArrayList<Album> albums;
    private String description;

    public Band() {
        this.albums = new ArrayList();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Band{" + "name=" + name + ", albums=" + albums + ", description=" + description + '}';
    }
}